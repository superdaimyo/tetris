﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager_Master : MonoBehaviour
{
    public delegate void GameManagerEventHandler();
    public event GameManagerEventHandler MainMenuToggleEvent;
    public event GameManagerEventHandler GameOverToggleEvent;
    
    public bool bGameOver;

    public static GameManager_Master instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        DontDestroyOnLoad(gameObject);
    }

    public void CallEventMainMenuToggle()
    {
        MainMenuToggleEvent?.Invoke();
    }

    public void CallEventGameOverToggle()
    {
        GameOverToggleEvent?.Invoke();
    }
 
}
