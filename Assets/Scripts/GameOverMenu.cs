﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    private GameManager_Master gameManager;

    void OnEnable()
    {
        SetInitialReferences();
    }

    void SetInitialReferences()
    {
        gameManager = GameObject.Find("GameStateManager").GetComponent<GameManager_Master>();
    }

    public void RetryGame()
    {
        gameManager.bGameOver = !gameManager.bGameOver;
        gameManager.CallEventGameOverToggle();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void QuitGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
    }
}
