﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Game : MonoBehaviour
{
    GameManager_Master gameManager;

    readonly int nTetrominoWidth = 4;
    public GameObject gameOverUI;

    string[] tetromino = new string[7];
    int nScreenWidth = 640;			// Console Screen Size X (columns)
    int nScreenHeight = 960;			// Console Screen Size Y (rows)
    int nFieldWidth = 12;
    int nFieldHeight = 20;
    int[] pieceField;
    int[] holdField;
    int[] nextField;
    int nHoldPiece = -1;
    float ppu = 32;

    bool bForceDown;
    bool bHoldLock;

    int nCurrentPiece = 0;
    int nNextPiece = 0;
	int nCurrentRotation = 0;

    float fSpeed = 1f;
	float elapsedTime = 0f;
	int nCurrentX = 0;
	int nCurrentY = 0;

    GameObject[] screen;
    GameObject[] holdBox;
    GameObject[] nextBox;
    string[] tileNameArray;

    System.Random rnd = new System.Random();
	List<int> vLines = new List<int>();

    int nScore;

    int nPieceCount;

    int nLineCount;

    bool bClearingLines;

    public TextMeshProUGUI lines;
    public TextMeshProUGUI score;
    void OnEnable()
    {
        gameManager = GameObject.Find("GameStateManager").GetComponent<GameManager_Master>();
        gameManager.GameOverToggleEvent += ToggleGameOver;
    }

    private void OnDisable()
    {
        gameManager.GameOverToggleEvent -= ToggleGameOver;
    }


    // Start is called before the first frame update
    void Start()
    {

        tileNameArray =  new string[] {
            "",
            "IPrefab",
            "TPrefab",
            "OPrefab",
            "SPrefab",
            "ZPrefab",
            "LPrefab",
            "JPrefab",
            "WallPrefab",
            "WallPrefab",
        };

        lines.SetText(string.Format("Lines: {0}", nLineCount));
        score.SetText(string.Format("Score: {0}", nScore));

        // Create Screen Buffer
        screen = new GameObject[nFieldWidth*nFieldHeight];
        holdBox = new GameObject[nTetrominoWidth*nTetrominoWidth];
        nextBox = new GameObject[nTetrominoWidth*nTetrominoWidth];

        for (int i = 0; i < nFieldWidth*nFieldHeight; i++) screen[i] = null;

        tetromino[0] = "..X...X...X...X."; // I
        tetromino[1] = "..X..XX...X....."; // T
        tetromino[2] = ".....XX..XX....."; // O
        tetromino[3] = "..X..XX..X......"; // S
        tetromino[4] = ".X...XX...X....."; // Z
        tetromino[5] = ".X...X...XX....."; // L
        tetromino[6] = "..X...X..XX....."; // J

        pieceField = new int[nFieldWidth*nFieldHeight];
        holdField = new int[nTetrominoWidth*nTetrominoWidth];
        nextField = new int[nTetrominoWidth*nTetrominoWidth];

	    for (int x = 0; x < nFieldWidth; x++) // Board Boundary
		    for (int y = 0; y < nFieldHeight; y++)
			    pieceField[y*nFieldWidth + x] = (x == 0 || x == nFieldWidth - 1 || y == nFieldHeight - 1) ? 9 : 0;

        DrawField();
        nCurrentX = (nFieldWidth / 2) - nTetrominoWidth/2;

        nCurrentPiece = rnd.Next(7);
        nNextPiece = rnd.Next(7);
        DrawNextPiece();
    }
             
    private void Update() {

        if(!gameManager.GetComponent<GameManager_Master>().bGameOver && !bClearingLines) {

		    // Input ========================
            if(Input.GetKeyDown(KeyCode.LeftArrow)){
		        nCurrentX -= (PathClear(nCurrentPiece, nCurrentRotation, nCurrentX - 1, nCurrentY)) ? 1 : 0;
            }

            if(Input.GetKeyDown(KeyCode.RightArrow)){
                nCurrentX += (PathClear(nCurrentPiece, nCurrentRotation, nCurrentX + 1, nCurrentY)) ? 1 : 0;
            }
            
            if(Input.GetKeyDown(KeyCode.DownArrow)){
		        nCurrentY += (PathClear(nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY + 1)) ? 1 : 0;
            }

            if(Input.GetKeyDown(KeyCode.Z)) {
			    nCurrentRotation += (PathClear(nCurrentPiece, nCurrentRotation + 1, nCurrentX, nCurrentY)) ? 1 : 0;
            }

            if(Input.GetKeyDown(KeyCode.X)) {
                if (!bHoldLock)
                {
                    bHoldLock = !bHoldLock;

                    // Pick New Piece
                    nCurrentX = nFieldWidth / 2 - nTetrominoWidth/2;
                    nCurrentY = 0;
                    nCurrentRotation = 0;

                    var tmp = nHoldPiece;
                    nHoldPiece = nCurrentPiece;

                    if (tmp >= 0)
                    {
                       nCurrentPiece = tmp; 
                    } else {                                          
                       // Hold piece
                       nCurrentPiece = nNextPiece;
                       nNextPiece = rnd.Next(7);
                    }
                    DrawHeldPiece();
                    DrawNextPiece();
                }
   
            }

            // Force the piece down the playfield if it's time
            if (bForceDown)
            {
                // Update difficulty every 50 pieces
                nPieceCount++;
                if (nPieceCount % 50 == 0)
				    if (fSpeed >= 0.1f) fSpeed -= 0.1f;

                elapsedTime = 0;

                // Test if piece can be moved down
			    if (PathClear(nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY + 1))
				    nCurrentY++;
                else {
                    //Lock the piece in place
                    for (int x = 0; x < nTetrominoWidth; x++)
                        for (int y = 0; y < 4; y++)
                            if (tetromino[nCurrentPiece][Rotate(x, y, nCurrentRotation)] != '.')
                                pieceField[(nCurrentY + y) * nFieldWidth + (nCurrentX + x)] = nCurrentPiece + 1;

                    // Check for lines
                    for (int y = 0; y < 4; y++)
                        if(nCurrentY + y < nFieldHeight - 1)
                        {
                            bool bLine = true;
                            for (int x = 1; x < nFieldWidth - 1; x++)
                            	bLine &= (pieceField[(nCurrentY + y) * nFieldWidth + x]) != 0;

                            if (bLine)
                            {
                            	for (int x = 1; x < nFieldWidth - 1; x++)
                            		pieceField[(nCurrentY + y) * nFieldWidth + x] = 8;
                            	vLines.Add(nCurrentY + y);
                            }						
                        }

                    nScore += 25;
                    if(vLines.Count > 0) nScore += (1 << vLines.Count) * 100;
                    nLineCount += vLines.Count;

                    lines.SetText(string.Format("Lines: {0}", nLineCount));
                    score.SetText(string.Format("Score: {0}", nScore));
                    
                    // Pick New Piece
                    nCurrentX = (nFieldWidth / 2) - nTetrominoWidth/2;
                    nCurrentY = 0;
                    nCurrentRotation = 0;
                    bHoldLock = false;

                    nCurrentPiece = nNextPiece;
                    nNextPiece = rnd.Next(7);
                    DrawNextPiece();
                    // If piece does not fit immediately game over!
                    gameManager.bGameOver = !PathClear(nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY);
                    if (gameManager.bGameOver)
                    {
                        while(!PathClear(nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY))
                            nCurrentY--;
                        DrawPiece();

                        gameManager.CallEventGameOverToggle();

                    }

                }
            }


            // Display ======================
            DrawField();

            // Remove lines
            StartCoroutine(ClearLines());

            // Draw Current Piece
            if(!bClearingLines)
                DrawPiece();
        }

    }

    void DrawPiece() {
        int screenOffset;
        for (int x = 0; x < 4; x++)
            for (int y = 0; y < 4; y++)
                if (tetromino[nCurrentPiece][Rotate(x, y, nCurrentRotation)] != '.') {
                    screenOffset = (nCurrentY + y)*nFieldWidth + (nCurrentX + x);
                    if (screenOffset > 0 && screenOffset < screen.Length)
                        if (screen[screenOffset] == null ||
                            screen[screenOffset].name != tileNameArray[nCurrentPiece + 1] 
                        )
                        {
                            screen[screenOffset] = GameObject.Instantiate(Resources.Load("Prefabs/"+ tileNameArray[nCurrentPiece+1])) as GameObject;
                            screen[screenOffset].name = tileNameArray[nCurrentPiece+1];
                            screen[screenOffset].transform.position = new Vector3(
                            nCurrentX + x - nScreenWidth/(4f*ppu) - 0.5f,
                            (-y  + nScreenHeight/(6f*ppu) - 0.5f) - nCurrentY, 
                            0); 
                        }

                }  
    }

    IEnumerator ClearLines() {
        if (vLines.Count > 0)
        {
            bClearingLines = true;
            yield return new WaitForSeconds(0.5f);
            foreach (var line in vLines) 
                for (int positionX = 1; positionX < nFieldWidth - 1; positionX++) {
                    for (int positionY = line; positionY > 0; positionY--) {
                        pieceField[positionY * nFieldWidth + positionX] = pieceField[(positionY - 1) * nFieldWidth + positionX];
                    }
                    pieceField[positionX] = 0;
                } 
            vLines.Clear();
        }
        bClearingLines = false;
        yield return null;
    }
    
    private void FixedUpdate() {

        elapsedTime += Time.deltaTime;
        bForceDown = (elapsedTime >= fSpeed);

    }

    int Rotate(int x, int y, int rotation)
    {
        int index = 0;
        switch (rotation % 4)
        {
            case 0: // 0 degrees
                index = y * 4 + x;
                break;

            case 1: // 90 degrees
                index = 12 + y - (x * 4);
                break;

            case 2: // 180 degrees
                index = 15 - (y * 4) - x;
                break;

            case 3: // 270 degrees
                index = 3 - y + (x * 4);
                break;
        }

        return index;
    }

    bool PathClear(int nTetromino, int nRotation, int nPositionX, int nPositionY)
    {
        // Occupied if element > zero
        for (int x = 0; x < 4; x++)
            for (int y = 0; y < 4; y++)
            {
                // Get index into piece
                int pieceIndex = Rotate(x, y, nRotation);

                // Get index into field
                int fieldIndex = (nPositionY + y) * nFieldWidth + (nPositionX + x);

                // Check that test is in bounds.
                if (nPositionX + x >= 0 && nPositionX + x < nFieldWidth)
                {
                    if (nPositionY + y >= 0 && nPositionY + y < nFieldHeight)
                    {
                        // In Bounds so do collision check
                        if (tetromino[nTetromino][pieceIndex] != '.' && pieceField[fieldIndex] != 0)
                            return false; // fail on first hit
                    }
                }
            }

        return true;
    }

    void DrawField() {
        for (int x = 0; x < nFieldWidth; x++)
            for (int y = 0; y < nFieldHeight; y++) {

                if ( screen[y*nFieldWidth + x] == null ||
                     screen[y*nFieldWidth + x].name != tileNameArray[pieceField[y*nFieldWidth + x]])
                {
                    if (screen[y*nFieldWidth + x] != null)
                    {
                        Destroy(screen[y*nFieldWidth + x].gameObject);
                        screen[y*nFieldWidth + x] = null;
                    }

                    if (tileNameArray[pieceField[y*nFieldWidth + x]] == "")
                        continue;

                    screen[y*nFieldWidth + x] = GameObject.Instantiate(Resources.Load("Prefabs/"+ tileNameArray[pieceField[y*nFieldWidth + x]])) as GameObject;
                    screen[y*nFieldWidth + x].name = tileNameArray[pieceField[y*nFieldWidth + x]];
                    screen[y*nFieldWidth + x].transform.position = new Vector3(
                    x - nScreenWidth/(4f*ppu) - 0.5f, 
                    - y + nScreenHeight/(6f*ppu) - 0.5f, 
                    0);
                }
            }
    }
    void DrawNextPiece() {
         // Draw Next Piece
        for (int x = 0; x < 4; x++)
            for (int y = 0; y < 4; y++) {
                        if (nextBox[(y)*nTetrominoWidth + (x)] == null ||
                            nextBox[(y)*nTetrominoWidth + (x)].name != tileNameArray[nNextPiece + 1] 
                        )
                        {
                            if (nextBox[(y)*nTetrominoWidth + (x)] != null) {
                                Destroy(nextBox[y*nTetrominoWidth + x].gameObject);
                                nextBox[y*nTetrominoWidth + x] = null;
                            }   
                            if (tileNameArray[nextField[y*nTetrominoWidth + x]] == "" && tetromino[nNextPiece][Rotate(x, y, 0)] == '.')
                                continue;         

                            nextBox[(y)*nTetrominoWidth + (x)] = GameObject.Instantiate(Resources.Load("Prefabs/"+ tileNameArray[nNextPiece+1])) as GameObject;
                            nextBox[(y)*nTetrominoWidth + (x)].name = tileNameArray[nNextPiece+1];
                            nextBox[(y)*nTetrominoWidth + (x)].transform.position = new Vector3(
                            x + nScreenWidth/(2f*ppu) - 0.5f,
                            (-y  + nScreenHeight/(6f*ppu) - 0.5f), 
                            0); 
                        }
            }
    }

    void DrawHeldPiece() {
         // Draw Current Piece
        for (int x = 0; x < 4; x++)
            for (int y = 0; y < 4; y++) {
                        if (holdBox[(y)*nTetrominoWidth + (x)] == null ||
                            holdBox[(y)*nTetrominoWidth + (x)].name != tileNameArray[nHoldPiece + 1] 
                        )
                        {
                            if (holdBox[(y)*nTetrominoWidth + (x)] != null) {
                                Destroy(holdBox[y*nTetrominoWidth + x].gameObject);
                                holdBox[y*nTetrominoWidth + x] = null;
                            }   
                            if (tileNameArray[holdField[y*nTetrominoWidth + x]] == "" && tetromino[nHoldPiece][Rotate(x, y, 0)] == '.')
                                continue;         

                            holdBox[(y)*nTetrominoWidth + (x)] = GameObject.Instantiate(Resources.Load("Prefabs/"+ tileNameArray[nHoldPiece+1])) as GameObject;
                            holdBox[(y)*nTetrominoWidth + (x)].name = tileNameArray[nHoldPiece+1];
                            holdBox[(y)*nTetrominoWidth + (x)].transform.position = new Vector3(
                            x - nScreenWidth/(2f*ppu) - 0.5f,
                            (-y  + nScreenHeight/(6f*ppu) - 0.5f), 
                            0); 
                        }
            }
    }

    void ToggleGameOver() {

        gameOverUI.SetActive(!gameOverUI.activeSelf);
    }


}
